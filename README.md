# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Here Weather application lives.
It queries OpenWeather.org site for weather in cities, chosen by selector, parses received JSON objects, shows it in interface and saves in SQLite Data Base - if there is no connection, then it queries Data Base for weather information.

It also shows example of Unreal Networking - there is an effects trigger, when server local actor enters - effects activation events are sent to clients, when client local actor enters trigger - it calls RPC to server and then events are replicated to clients.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Setting input mode to interface or 3D-world mode - right Mouse Button Click.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact