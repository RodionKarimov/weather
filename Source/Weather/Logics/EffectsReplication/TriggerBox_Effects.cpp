// Fill out your copyright notice in the Description page of Project Settings.

#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Character.h"
#include "TriggerBox_Effects.h"



ATriggerBox_Effects :: ATriggerBox_Effects ( const FObjectInitializer& ObjectInitializer ) : Super ( ObjectInitializer ) {
	m_pParticlesSystem = ObjectInitializer.CreateDefaultSubobject<UParticleSystemComponent>(this, TEXT("ParticlesSystem"));
	m_pParticlesSystem -> bAutoActivate = false;
	m_pParticlesSystem -> bAutoDestroy = false;
	m_pParticlesSystem -> SetupAttachment ( RootComponent );

	bReplicates                            = true;
	bAlwaysRelevant                        = true;

}

void ATriggerBox_Effects :: BeginPlay () {
	FVector                               vLocation;



	Super :: BeginPlay ();



	// if ( m_templateParticles == NULL ) {
	// 	UE_LOG ( LogTemp, Error, TEXT ( "ATriggerBox_Effects.BeginPlay () : m_templateParticles is not set." ) );

	// } else {
	// 	vLocation                           = GetActorLocation ();
	// 	// vLocation.Z                         = vLocation.Z + 60.0f;
	// 	m_pParticlesSystem                  = UGameplayStatics :: SpawnEmitterAttached (
	// 		m_templateParticles,
	// 		RootComponent,
	// 		NAME_None,
	// 		vLocation,
	// 		GetActorRotation (),
	// 		EAttachLocation :: KeepWorldPosition,
	// 		false
	// 	);
	// 	//m_pParticlesSystem -> DeactivateSystem ();
	                                           
	// } //-else

	GetCollisionComponent () -> OnComponentBeginOverlap.AddDynamic(this, &ATriggerBox_Effects::OnOverlapBegin); // GetCollisionComponent ()
	GetCollisionComponent () -> OnComponentEndOverlap.AddDynamic(this, &ATriggerBox_Effects::OnOverlapEnd);

	SetOwner ( GetWorld () -> GetFirstPlayerController () );

}



void ATriggerBox_Effects :: OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	// AStanzzaVisualization_PC * pPC;


	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.OnOverlapBegin () : Actor entered trigger : %s , %i, %i ." ),
		* OtherActor -> GetClass () -> GetName (),
		GetLocalRole () == ROLE_Authority,
		Cast < ACharacter > ( OtherActor ) -> IsLocallyControlled ()
	);

	// // if ( m_bWasActivated )
	// // 	return;

	// if ( OtherActor && ( OtherActor != this ) && OtherComp &&
	//      ( Cast < ACharacter > ( OtherActor ) != nullptr ) ) {

	// 	// if ( m_pDoor != nullptr )
	// 	// 	m_pDoor -> Open ( ! m_pDoor -> m_bIsOpen );
	// 	// else
	// 	// 	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.OnOverlapBegin () : %s : m_pDoor is not set." ), * GetName () );

	// 	// //m_bWasActivated = true;

	// 	pPC = Cast < AStanzzaVisualization_PC > ( Cast < ACharacter > ( OtherActor ) -> Controller );

	// 	if ( pPC ) {
	// 		pPC -> SetRoomTrigger ( this );
	// 		pPC -> SetFurnitureSolutions ( m_aFurnitureSolutions );
	// 		pPC -> SetActiveFurnitureSolution ( m_pActiveFurnitureSolution );
	// 	} //-if

	// } //-if

	if ( OtherActor && ( OtherActor != this ) && OtherComp && OtherActor -> GetClass () -> IsChildOf ( ACharacter :: StaticClass () ) ) {
		SetOwner ( Cast < ACharacter > ( OtherActor ) -> Controller );

		if ( Cast < ACharacter > ( OtherActor ) -> IsLocallyControlled () ) {
			// m_amountActivations = m_amountActivations + 1;
			// m_pParticlesSystem -> ActivateSystem ();

			// if ( GetLocalRole () == ROLE_Authority ) {

			// } else {
			// 	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.OnOverlapBegin () : %s - Calling server to apply activations." ),
			// 		* OtherActor -> GetClass () -> GetName ()
			// 	);
			// 	Server_ApplyActivation ( m_amountActivations );
			// }

			delegateTimerActivation.BindUFunction ( this, FName ( "TimerEvent_Activate" ), true );
			GetWorld () -> GetTimerManager ().SetTimer ( TimerHandle_Activation, delegateTimerActivation, m_fActivation_TimeInterval, false );

		}

	}

}

void ATriggerBox_Effects :: OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.OnOverlapEnd () : Actor exited trigger : %s ." ), * OtherActor -> GetClass () -> GetName () );

	if ( OtherActor && ( OtherActor != this ) && OtherComp && OtherActor -> GetClass () -> IsChildOf ( ACharacter :: StaticClass () ) ) {

		SetOwner ( Cast < ACharacter > ( OtherActor ) -> Controller );

		if ( Cast < ACharacter > ( OtherActor ) -> IsLocallyControlled () ) {
			// m_amountActivations = m_amountActivations - 1;
			// m_pParticlesSystem -> DeactivateSystem ();

			// if ( GetLocalRole () == ROLE_Authority ) {

			// } else {
			// 	Server_ApplyActivation ( m_amountActivations );
			// }

			delegateTimerActivation.BindUFunction ( this, FName ( "TimerEvent_Activate" ), false );
			GetWorld () -> GetTimerManager ().SetTimer ( TimerHandle_Activation, delegateTimerActivation, m_fActivation_TimeInterval, false );

		}

	} //-if

}

void ATriggerBox_Effects :: TimerEvent_Activate ( bool _bActivate ) {
	if ( _bActivate ) {
		m_amountActivations = m_amountActivations + 1;
		m_pParticlesSystem -> ActivateSystem ();
	} else {
		m_amountActivations = m_amountActivations - 1;
		m_pParticlesSystem -> DeactivateSystem ();
	}

	if ( GetLocalRole () == ROLE_Authority ) {

	} else {
		UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.TimerEvent_Activate () : Calling server to apply activations : %i ." ),
			_bActivate
		);
		Server_ApplyActivation ( m_amountActivations );
	}
}



//---Networking.------------------------------------------------------------------------
void ATriggerBox_Effects :: GetLifetimeReplicatedProps ( TArray < FLifetimeProperty > & OutLifetimeProps ) const {
  Super :: GetLifetimeReplicatedProps ( OutLifetimeProps );

  DOREPLIFETIME ( ATriggerBox_Effects, m_amountActivations );

}



void ATriggerBox_Effects :: OnRep_amountActivations () {
	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.OnRep_amountActivations () : Amount of activations has been replicated : %i : %i ." ),
		m_amountActivations, m_amountActivationsPrevious
	);

	// if ( pParticlesSystem != NULL )
	// 	if ( bActive ) pParticlesSystem -> ActivateSystem ();
	// 	else pParticlesSystem -> DeactivateSystem ();

	ApplyActivation ( m_amountActivations );
	m_amountActivationsPrevious = m_amountActivations;

}



void ATriggerBox_Effects :: ApplyActivation ( int _amountActivations ) {
	if ( _amountActivations > m_amountActivationsPrevious ) {
		m_pParticlesSystem -> ActivateSystem ();

	} else {
		m_pParticlesSystem -> DeactivateSystem ();
	}
}


void ATriggerBox_Effects :: Server_ApplyActivation_Implementation ( int _amountActivations ) {
	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Effects.Server_ApplyActivation_Implementation () : Application of amount of activations has been called : %i : %i ." ),
		_amountActivations, m_amountActivationsPrevious
	);

	ApplyActivation ( _amountActivations );
	m_amountActivations = _amountActivations;
	m_amountActivationsPrevious = m_amountActivations;

}

bool ATriggerBox_Effects :: Server_ApplyActivation_Validate ( int _amountActivations ) { return true; }
