// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "TriggerBox_Effects.generated.h"

/**
 * 
 */
UCLASS()
class WEATHER_API ATriggerBox_Effects : public ATriggerBox
{
	GENERATED_BODY()



	virtual void BeginPlay () override;



public :
	ATriggerBox_Effects ( const FObjectInitializer& ObjectInitializer );



	// UPROPERTY ( EditAnywhere, Category = "Effects" )
	// UParticleSystem *                      m_templateParticles;
	
	UPROPERTY ( EditAnywhere, Category = "Effects" )
	UParticleSystemComponent *             m_pParticlesSystem;



	/** called when something enters the sphere component */
    UFUNCTION ()
    void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    /** called when something leaves the sphere component */
    UFUNCTION()
    void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);



    FTimerHandle TimerHandle_Activation;
	float m_fActivation_TimeInterval = 0.2f;
	UFUNCTION ()
	void TimerEvent_Activate ( bool _bActivate );
	FTimerDelegate delegateTimerActivation;



    UPROPERTY ( Replicated, ReplicatedUsing = OnRep_amountActivations )
	int                            m_amountActivations;
	int                            m_amountActivationsPrevious;

	void ApplyActivation ( int _amountActivations );

	UFUNCTION ( reliable, server, WithValidation )
    void Server_ApplyActivation ( int _amountActivations );
	
	UFUNCTION ()
	virtual void OnRep_amountActivations ();
	
};
