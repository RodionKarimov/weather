// Fill out your copyright notice in the Description page of Project Settings.

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Blueprint/UserWidget.h"
#include "UI/GUI/Weather/W_Weather.h"
#include "PCWeather.h"



APCWeather :: APCWeather () {
	TArray < AActor * > aActors;

	// if ( GetNetMode () == NM_Client || GetNetMode () == NM_Standalone ) {   // NM_ListenServer, NM_DedicatedServer
	// if ( GetLocalRole () == ROLE_Authority ) {
	if ( IsLocalController () ) {
		aActors.Empty ();
		UGameplayStatics :: GetAllActorsOfClass ( ( const UObject * ) ( this -> GetWorld () ), AWeatherHandler :: StaticClass (), aActors );
		if ( aActors.Num () == 0 ) {
			UE_LOG ( LogTemp, Warning, TEXT ( "APCWeather.AWeatherGameMode () : There is no AWeatherHandler object in the scene." ) );
			return;
		} else {
			m_pWeatherHandler = Cast < AWeatherHandler > ( aActors [ 0 ] );
		}

	}

	aActors.Empty ();
	UGameplayStatics :: GetAllActorsOfClass ( ( const UObject * ) ( this -> GetWorld () ), ASQLiteHandlerActor :: StaticClass (), aActors );
	if ( aActors.Num () == 0 ) {
		UE_LOG ( LogTemp, Warning, TEXT ( "APCWeather.AWeatherGameMode () : There is no ASQLiteHandlerActor object in the scene." ) );
		return;
	} else {
		m_pSQLiteHandlerActor = Cast < ASQLiteHandlerActor > ( aActors [ 0 ] );
	}

	if ( m_pSQLiteHandlerActor ) {
		m_pSQLiteHandlerActor -> Initialize ();
	}

}

void APCWeather :: PostInitializeComponents () {
	Super :: PostInitializeComponents ();
}

void APCWeather::BeginPlay() {
	// m_logicsApplication = Cast < AWeatherGameMode > ( GetWorld () -> GetAuthGameMode () );

	// if ( GetNetMode () == NM_Client || GetNetMode () == NM_Standalone ) {
	// if ( GetLocalRole () == ROLE_Authority ) {
	if ( IsLocalController () ) {
		initialize ();

		if ( m_pWeatherHandler ) {
			m_pWeatherHandler -> Initialize ( this );
		}

		if (m_pWeatherWidget) {
			m_pWeatherWidget->AddToViewport();
			m_pWeatherWidget->SetVisibility(ESlateVisibility::Visible);
		} else {
			UE_LOG ( LogTemp, Error, TEXT ( "APCWeather.BeginPlay () : %s : Weather widget was not created." ),
				* GetName ()
			);
		}

	}

	Super::BeginPlay ();
}

void APCWeather :: initialize () {
	FName name = FName(TEXT("WeatherWidget"));

	UE_LOG ( LogTemp, Log, TEXT ( "APCWeather.initialize () : %s : Creating weather widget." ),
		* GetName ()
	);

	m_pWeatherWidget = CreateWidget<UW_Weather>(this, m_classWeatherWidget, name);
}



void APCWeather :: SetupInputComponent () {
	Super::SetupInputComponent();

	// UI input
	InputComponent->BindAction("SwitchBetweenInterfaceAndSceneInputModes", IE_Pressed, this, &APCWeather::SwitchBetweenInterfaceAndSceneInputModes);

}

void APCWeather :: SwitchBetweenInterfaceAndSceneInputModes () {
	m_bShowMouseCursor = ! m_bShowMouseCursor;
	EnableInteractionWithInterface ( m_bShowMouseCursor );
}

void APCWeather :: EnableInteractionWithInterface ( bool _bEnable ) {
	//UE_LOG ( LogTemp, Log, TEXT ( "APCWeather.EnableInteractionWithInterface () : %i." ), _bEnable );
	
	if ( _bEnable ) {
		UWidgetBlueprintLibrary:: SetInputMode_GameAndUI (this); // SetInputMode_UIOnlyEx
		bShowMouseCursor = true;
		bEnableClickEvents = true;
		bEnableMouseOverEvents = true;
	
		//pW_InGameMenu -> SetVisibility ( ESlateVisibility :: Visible );
	
	} else {
		UWidgetBlueprintLibrary::SetInputMode_GameOnly(this);
		bShowMouseCursor = false;
		bEnableClickEvents = false;
		bEnableMouseOverEvents = false;

		//pW_InGameMenu -> SetVisibility ( ESlateVisibility :: Collapsed );

	}
	
}



void APCWeather :: UpdateCity ( FString _sCity ) {
	UE_LOG ( LogTemp, Log, TEXT ( "APCWeather.UpdateCity () : %s : Updating city : %s ." ),
		* GetName (), * _sCity
	);

	m_pWeatherWidget -> ClearWeather ();
	// m_logicsApplication -> Get_WeatherHandler () -> UpdateCity ( _sCity );
	m_pWeatherHandler -> UpdateCity ( _sCity );
}

void APCWeather :: SetWeather (
	float _temperature, float _temperatureFeelsLike, float _temperatureMin, float _temperatureMax,
	float _pressure, float _humidity,
	FString _description,
	FString _sWeatherMain, FString _sWeatherMainDescription,
	float _fVisibility,
	float _fWindSpeed, float _fWindDirection,
	int _iCloudsAll,
	FString _sDateAndTime
) {
	// UE_LOG ( LogTemp, Log, TEXT ( "APCWeather.SetWeather () : %s : Setting weather : %i ." ),
	// 	* GetName (), m_pWeatherWidget
	// );

	if ( m_pWeatherWidget ) {
		m_pWeatherWidget -> SetWeather (
			_temperature, _temperatureFeelsLike, _temperatureMin, _temperatureMax,
			_pressure, _humidity,
			_description,
			_sWeatherMain, _sWeatherMainDescription,
			_fVisibility,
			_fWindSpeed, _fWindDirection,
			_iCloudsAll,
			_sDateAndTime
		);
	}
}
