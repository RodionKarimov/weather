// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "WeatherGameMode.h"
#include "Logics/Weather/WeatherHandler.h"
#include "System/Data/DataBases/SQLite/SQLiteHandlerActor.h"
#include "PCWeather.generated.h"

class UW_Weather;

/**
 * 
 */
UCLASS()
class WEATHER_API APCWeather : public APlayerController
{
	GENERATED_BODY()

	APCWeather();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

	void SetupInputComponent ();

	// AWeatherGameMode * m_logicsApplication;
	AWeatherHandler * m_pWeatherHandler;
	ASQLiteHandlerActor * m_pSQLiteHandlerActor;

	bool m_bShowMouseCursor = false;

public:

	void initialize ();

	AWeatherHandler * Get_WeatherHandler () { return m_pWeatherHandler; }
	ASQLiteHandlerActor * Get_SQLiteHandlerActor () { return m_pSQLiteHandlerActor; }

	UPROPERTY ( EditAnywhere, BlueprintReadWrite, Category = "Widgets" )
    TSubclassOf < class UUserWidget > m_classWeatherWidget;

    UW_Weather * m_pWeatherWidget;

    void SwitchBetweenInterfaceAndSceneInputModes ();
    void EnableInteractionWithInterface ( bool _bEnable );

    void UpdateCity ( FString _sCity );

    void SetWeather (
    	float _temperature, float _temperatureFeelsLike, float _temperatureMin, float _temperatureMax,
		float _pressure, float _humidity,
    	FString _description,
    	FString _sWeatherMain, FString _sWeatherMainDescription,
    	float _fVisibility,
		float _fWindSpeed, float _fWindDirection,
		int _iCloudsAll,
		FString _sDateAndTime
    );
};
