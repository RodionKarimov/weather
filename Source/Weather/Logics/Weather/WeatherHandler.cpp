// Fill out your copyright notice in the Description page of Project Settings.

#include "JsonObjectConverter.h"
#include "WeatherGameMode.h"
#include "Logics/PCWeather.h"
#include "WeatherHandler.h"



const TArray < FString > g_columns = {
	"city",
	"type",
	"temperature", "temperatureFeelsLike", "temperatureMin", "temperatureMax",
	"pressure", "humidity",
	"sWeatherMain", "sWeatherMainDescription",
	"fVisibility",
	"fWindSpeed", "fWindDirection",
	"iCloudsAll",
	"sDateAndTime"
};



APCWeather * g_pPC;



static int callbackGetDataFromDataBase(void *data, int argc, char **argv, char **azColName){
	TMap < FString, FString > mapWeatherValues;

	int i;
	// int j;

	// fprintf(stderr, "%s: ", (const char*)data);

	// UE_LOG ( LogTemp, Log, TEXT ( "AWeatherHandler : callbackGetDataFromDataBase () : %i ." ), argc );

	for(i = 0; i<argc; i++){
		// printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		// UE_LOG ( LogTemp, Log, TEXT ( "AWeatherHandler : callbackGetDataFromDataBase () : %s - %s ." ), * FString ( azColName[i] ), * ( argv[i] ? FString ( argv[i] ) : "NULL" ) );
		mapWeatherValues.Add ( FString ( azColName[i] ), ( argv[i] ? FString ( argv[i] ) : "NULL" ) );
	}

	if ( g_pPC ) {
		g_pPC -> SetWeather (
			FCString :: Atof ( * mapWeatherValues [ "temperature" ] ), FCString :: Atof ( * mapWeatherValues [ "temperatureFeelsLike" ] ),
				FCString :: Atof ( * mapWeatherValues [ "temperatureMin" ] ), FCString :: Atof ( * mapWeatherValues [ "temperatureMax" ] ),
			FCString :: Atof ( * mapWeatherValues [ "pressure" ] ), FCString :: Atof ( * mapWeatherValues [ "humidity" ] ),
			"Weather description",
			mapWeatherValues [ "sWeatherMain" ], mapWeatherValues [ "sWeatherMainDescription" ],
			FCString :: Atof ( * mapWeatherValues [ "fVisibility" ] ),
			FCString :: Atof ( * mapWeatherValues [ "fWindSpeed" ] ), FCString :: Atof ( * mapWeatherValues [ "fWindDirection" ] ),
			FCString :: Atoi ( * mapWeatherValues [ "iCloudsAll" ] ),
			mapWeatherValues [ "sDateAndTime" ]
		);
	}

	// printf("\n");
	return 0;
}



// Sets default values
AWeatherHandler::AWeatherHandler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWeatherHandler::BeginPlay()
{
	Super::BeginPlay();

	// m_logicsApplication = Cast < AWeatherGameMode > ( GetWorld () -> GetAuthGameMode () );

	m_pHTTP                                   = & FHttpModule :: Get ();
}

// Called every frame
void AWeatherHandler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void AWeatherHandler :: Initialize ( APCWeather * _pPC ) {
	// m_logicsApplication = _logicsApplication;
	m_pPC                                     = _pPC;

	g_pPC                                     = Cast < APCWeather > ( GetWorld()->GetGameInstance()->GetFirstLocalPlayerController() );

	m_pPC -> Get_SQLiteHandlerActor () -> CreateTable (
		"tableWeather",
		g_columns
	);

	TimerEvent_GetWeather ();
	GetWorld () -> GetTimerManager ().SetTimer (
		TimerHandle_GetWeather,
		this,
		& AWeatherHandler :: TimerEvent_GetWeather,
		m_fGetWeather_TimeInterval,
		true );

	GetWorld () -> GetTimerManager ().SetTimer (
		TimerHandle_Initialization,
		this,
		& AWeatherHandler :: TimerEvent_Initialization,
		m_fInitialization_TimeInterval,
		false );
}

void AWeatherHandler :: TimerEvent_Initialization () {
	m_pPC -> Get_SQLiteHandlerActor () -> GetNewestValuesByCity ( "tableWeather", m_sActiveCity, callbackGetDataFromDataBase );
}



void AWeatherHandler :: UpdateCity ( FString _sCity ) {
	UE_LOG ( LogTemp, Log, TEXT ( "AWeatherHandler.UpdateCity () : %s : Updating city : %s ." ),
		* GetName (), * _sCity
	);

	m_sActiveCity = _sCity;
	TimerEvent_GetWeather ();
	m_pPC -> Get_SQLiteHandlerActor () -> GetNewestValuesByCity ( "tableWeather", m_sActiveCity, callbackGetDataFromDataBase );
}



void AWeatherHandler :: TimerEvent_GetWeather () {
	TSharedRef < IHttpRequest >           pRequest             = m_pHTTP -> CreateRequest ();
	pRequest -> OnProcessRequestComplete ().BindUObject ( this, & AWeatherHandler :: OnResponseReceived );
	//This is the url on which to process the request
	pRequest -> SetURL ( m_sServerURL + "?q=" + m_sActiveCity + "&units=metric&appid=" + m_sAPIKey );
	pRequest -> SetVerb ( "GET" );     // GET, POST
	pRequest -> SetHeader ( TEXT ( "User-Agent" ), "X-UnrealEngine-Agent" );
	pRequest -> SetHeader ( "Content-Type", TEXT ( "application/json" ) );
	pRequest -> ProcessRequest ();
}

void AWeatherHandler :: OnResponseReceived ( FHttpRequestPtr _pRequest, FHttpResponsePtr _pResponse, bool _bWasSuccessful ) {

	APCWeather * pPC;

	FWeather                               structureWeather;
	FString                                sWeather;

	FDateTime                              DateTime                             = FDateTime :: Now ();

	FString                                sDateAndTime;

	bool bResult = true;



	if ( ! _bWasSuccessful ) {
		UE_LOG ( LogTemp, Error, TEXT ( "AWeatherHandler.OnResponseReceived () : %s : There was error in get weather request processing." ),
			* GetName ()
		);

		return;
	}

	// UE_LOG ( LogTemp, Log, TEXT ( "AWeatherHandler.OnResponseReceived () : %s : %s ." ),
	// 	* GetName (),
	// 	* ( _pResponse -> GetContentAsString () )
	// );

	bResult = FJsonObjectConverter :: JsonObjectStringToUStruct (
	    _pResponse -> GetContentAsString (),
	    & structureWeather,
	    0,
	    0
	);

	if ( bResult ) {
		sDateAndTime                          = DateTime.ToString ();

		// FJsonObjectConverter :: UStructToJsonObjectString (
		// 	structureWeather,
		// 	sWeather,
		// 	0, 0, 1, nullptr, true
		// );

		// UE_LOG ( LogTemp, Log, TEXT ( "AWeatherHandler.OnResponseReceived () : %s : sWeather : %s ." ),
		// 	* GetName (),
		// 	* ( sWeather )
		// );



		const TArray < FString > & aValues = {
			m_sActiveCity,
			"OneDayForeCast",
			FString :: SanitizeFloat ( structureWeather.main.temp ), FString :: SanitizeFloat ( structureWeather.main.feels_like ),
				FString :: SanitizeFloat ( structureWeather.main.temp_min ), FString :: SanitizeFloat ( structureWeather.main.temp_max ),
			FString :: SanitizeFloat ( structureWeather.main.pressure ), FString :: SanitizeFloat ( structureWeather.main.humidity ),
			structureWeather.weather [ 0 ].main, structureWeather.weather [ 0 ].description,
			FString :: FromInt ( structureWeather.visibility ),
			FString :: SanitizeFloat ( structureWeather.wind.speed ), FString :: SanitizeFloat ( structureWeather.wind.deg ),
			FString :: FromInt ( structureWeather.clouds.all ),
			sDateAndTime
		};

		m_pPC -> Get_SQLiteHandlerActor () -> Insert (
			"tableWeather",
			g_columns,
			aValues
		);

		// m_pPC -> Get_SQLiteHandlerActor () -> GetNewestValues ( "tableWeather", aValues, callbackGetDataFromDataBase );
		// m_pPC -> Get_SQLiteHandlerActor () -> GetNewestValues ( "tableWeather", callbackGetDataFromDataBase );



		pPC                                       = Cast < APCWeather > ( GetWorld()->GetGameInstance()->GetFirstLocalPlayerController() );
		if ( pPC ) {
			pPC -> SetWeather (
				structureWeather.main.temp, structureWeather.main.feels_like, structureWeather.main.temp_min, structureWeather.main.temp_max,
				structureWeather.main.pressure, structureWeather.main.humidity,
				_pResponse -> GetContentAsString (),
				structureWeather.weather [ 0 ].main, structureWeather.weather [ 0 ].description,
				structureWeather.visibility,
				structureWeather.wind.speed, structureWeather.wind.deg,
				structureWeather.clouds.all,
				sDateAndTime
			);
		}

	} else {
		UE_LOG ( LogTemp, Error, TEXT ( "AWeatherHandler.OnResponseReceived () : %s : There was error in response JSON parsing : %s." ),
			* GetName (), * ( _pResponse -> GetContentAsString () )
		);
	}
}
