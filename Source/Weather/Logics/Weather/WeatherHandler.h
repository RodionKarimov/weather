// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "WeatherHandler.generated.h"



// class AWeatherGameMode;
class APCWeather;



USTRUCT ( BlueprintType, Blueprintable )
struct FCoordinates {
	GENERATED_BODY()

	UPROPERTY ( BlueprintReadOnly )
	int                                    lon;
	UPROPERTY ( BlueprintReadOnly )
	int                                    lat;


	FCoordinates () {
	}

};

USTRUCT ( BlueprintType, Blueprintable )
struct FAtmosphereParameters {
	GENERATED_BODY()

	UPROPERTY ( BlueprintReadOnly )
	int                                    id;
	UPROPERTY ( BlueprintReadOnly )
	FString                                main;
	UPROPERTY ( BlueprintReadOnly )
	FString                                description;
	UPROPERTY ( BlueprintReadOnly )
	FString                                icon;

	FAtmosphereParameters () {
	}

};

USTRUCT ( BlueprintType, Blueprintable )
struct FWeatherParameters {
	GENERATED_BODY()

	UPROPERTY ( BlueprintReadOnly )
	float                                  temp;
	UPROPERTY ( BlueprintReadOnly )
	float                                  feels_like;
	UPROPERTY ( BlueprintReadOnly )
	float                                  temp_min;
	UPROPERTY ( BlueprintReadOnly )
	float                                  temp_max;

	UPROPERTY ( BlueprintReadOnly )
	int                                    pressure;
	UPROPERTY ( BlueprintReadOnly )
	int                                    humidity;

	FWeatherParameters () {
	}

};

USTRUCT ( BlueprintType, Blueprintable )
struct FWindParameters {
	GENERATED_BODY()

	UPROPERTY ( BlueprintReadOnly )
	float                                  speed;
	UPROPERTY ( BlueprintReadOnly )
	float                                  deg;

	FWindParameters () {
	}

};

USTRUCT ( BlueprintType, Blueprintable )
struct FCloudsParameters {
	GENERATED_BODY()

	UPROPERTY ( BlueprintReadOnly )
	int                                    all;

	FCloudsParameters () {
	}

};

USTRUCT ( BlueprintType, Blueprintable )
struct FLocationParameters {
	GENERATED_BODY()

	UPROPERTY ( BlueprintReadOnly )
	int                                    type;
	UPROPERTY ( BlueprintReadOnly )
	int                                    id;
	UPROPERTY ( BlueprintReadOnly )
	FString                                country;
	UPROPERTY ( BlueprintReadOnly )
	int                                    sunrise;
	UPROPERTY ( BlueprintReadOnly )
	int                                    sunset;

	FLocationParameters () {
	}

};



USTRUCT ( BlueprintType, Blueprintable )                                // BlueprintType
struct FWeather {
	GENERATED_BODY()

	// {
	// 	"coord":{"lon":37.62,"lat":55.75},
	// 	"weather":[{"id":804,"main":"Clouds","description":"overcast clouds","icon":"04d"}],
	// 	"base":"stations",
	// 	"main":{"temp":23.61,"feels_like":25.05,"temp_min":22,"temp_max":24.44,"pressure":1018,"humidity":64},
	// 	"visibility":10000,
	// 	"wind":{"speed":1,"deg":0},
	// 	"clouds":{"all":98},
	// 	"dt":1592370332,
	// 	"sys":{"type":1,"id":9029,"country":"RU","sunrise":1592354658,"sunset":1592417803},
	// 	"timezone":10800,
	// 	"id":524901,
	// 	"name":"Moscow",
	// 	"cod":200
	// }
  
  	UPROPERTY ( BlueprintReadOnly )
	FCoordinates                            coord;
	UPROPERTY ( BlueprintReadOnly )
	TArray < FAtmosphereParameters >        weather;

	UPROPERTY ( BlueprintReadOnly )
	FString                                 base;

	UPROPERTY ( BlueprintReadOnly )
	FWeatherParameters                      main;

	UPROPERTY ( BlueprintReadOnly )
	int                                     visibility;

	UPROPERTY ( BlueprintReadOnly )
	FWindParameters                         wind;

	UPROPERTY ( BlueprintReadOnly )
	FCloudsParameters                       clouds;

	UPROPERTY ( BlueprintReadOnly )
	int                                     dt;

	UPROPERTY ( BlueprintReadOnly )
	FLocationParameters                     sys;

	UPROPERTY ( BlueprintReadOnly )
	int                                     timezone;
	UPROPERTY ( BlueprintReadOnly )
	int                                     id;
	UPROPERTY ( BlueprintReadOnly )
	FString                                 name;
	UPROPERTY ( BlueprintReadOnly )
	int                                     cod;

	FWeather () {
		// IsBuilt                             = false;
		// iObjectIndex                        = 0;
		// eObjectType                         = EObjectType :: Free;
		// pObject_2                           = NULL;
		// IsLightingEnabled                   = false;

	}

};



UCLASS()
class WEATHER_API AWeatherHandler : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeatherHandler();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// AWeatherGameMode * m_logicsApplication;
	APCWeather * m_pPC;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// void Initialize ( AWeatherGameMode * _logicsApplication );
	void Initialize ( APCWeather * _pPC );

	FHttpModule *                          m_pHTTP;

	void UpdateCity ( FString _sCity );

	UPROPERTY(Category="Weather", EditAnywhere)
	FString                                m_sServerURL       = "http://api.openweathermap.org/data/2.5/weather";
	UPROPERTY(Category="Weather", EditAnywhere)
	FString                                m_sActiveCity      = "Moscow";
	UPROPERTY(Category="Weather", EditAnywhere)
	FString                                m_sAPIKey          = "ba7976edbda9e4d949b8314f05c82fba";

	FTimerHandle TimerHandle_Initialization;
	float m_fInitialization_TimeInterval = 2.0f;
	void TimerEvent_Initialization ();

	FTimerHandle TimerHandle_GetWeather;
	float m_fGetWeather_TimeInterval = 4.0f;
	void TimerEvent_GetWeather ();

	void OnResponseReceived ( FHttpRequestPtr _pRequest, FHttpResponsePtr _pResponse, bool _bWasSuccessful );

};
