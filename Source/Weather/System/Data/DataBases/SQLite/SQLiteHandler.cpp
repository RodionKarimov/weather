// Fill out your copyright notice in the Description page of Project Settings.


#include "SQLiteHandler.h"

// Sets default values for this component's properties
USQLiteHandler::USQLiteHandler()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USQLiteHandler::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USQLiteHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

