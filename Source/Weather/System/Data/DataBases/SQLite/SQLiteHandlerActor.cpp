// Fill out your copyright notice in the Description page of Project Settings.

// #include "ThirdParty/Data/DataBases/sqlite3.h"
#include "sqlite3.h"
#include "SQLiteHandlerActor.h"



const char* SQL = "CREATE TABLE IF NOT EXISTS foo(a,b,c); INSERT INTO FOO VALUES(1,2,3); INSERT INTO FOO SELECT * FROM FOO;";



sqlite3 *db = 0; // хэндл объекта соединение к БД



static int callback(void *data, int argc, char **argv, char **azColName){
	int i;
	// int j;
	// fprintf(stderr, "%s: ", (const char*)data);

	// UE_LOG ( LogTemp, Log, TEXT ( "ASQLiteHandlerActor : callback () : %i ." ), argc );

	for(i = 0; i<argc; i++){
		// printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		// UE_LOG ( LogTemp, Log, TEXT ( "ASQLiteHandlerActor : callback () : %s - %s ." ), * FString ( azColName[i] ), * ( argv[i] ? FString ( argv[i] ) : "NULL" ) );
	}

	// printf("\n");
	return 0;
}



// Sets default values
ASQLiteHandlerActor::ASQLiteHandlerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASQLiteHandlerActor::BeginPlay()
{
	Super::BeginPlay();
}

void ASQLiteHandlerActor::BeginDestroy() {
	char *err = 0;

	sqlite3_free ( err );

	// закрываем соединение
	sqlite3_close(db);

	Super::BeginDestroy();

}

// Called every frame
void ASQLiteHandlerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void ASQLiteHandlerActor :: Initialize () {
	FString sPath = FPaths :: ProjectDir () + "Binaries/Data/DataBases/SQLite/weather.dblite";

	UE_LOG ( LogTemp, Log, TEXT ( "ASQLiteHandlerActor.Initialize () : %s : sPath : %s ." ), * GetName (), * sPath );

	// открываем соединение
	if( sqlite3_open( TCHAR_TO_ANSI ( * sPath ), &db) != SQLITE_OK ) {
		UE_LOG ( LogTemp, Error, TEXT ( "ASQLiteHandlerActor.Initialize () : %s : There was error in openning Data Base file in sPath : %s - location ." ), * GetName (), * sPath );
		// fprintf(stderr, "Ошибка открытия/создания БД: %s\n", sqlite3_errmsg(db));

	// выполняем SQL
	}
	// else if (sqlite3_exec(db, SQL, 0, 0, &err)) {
	// 	// fprintf(stderr, "Ошибка SQL: %sn", err);
	// 	// sqlite3_free ( err );
	// }
}



bool ASQLiteHandlerActor :: CreateTable ( const FString _name, const TArray < FString > & _aColumns ) {
	char *err = 0;

	FString sColumns = "( id INTEGER PRIMARY KEY NOT NULL, ";
	int i = 0;

	if ( _aColumns.Num () > 0 ) {
		sColumns = sColumns + _aColumns [ 0 ];
	}

	for ( i = 1; i < _aColumns.Num (); i ++ ) {
		sColumns = sColumns + ", " + _aColumns [ i ];
	}

	sColumns = sColumns + " )";

	const FString query = "CREATE TABLE IF NOT EXISTS " + _name + " " + sColumns + ";";

	UE_LOG ( LogTemp, Log, TEXT ( "ASQLiteHandlerActor.CreateTable () : %s : Creating table : %s : %s ." ), * GetName (), * _name, * query );

	if ( sqlite3_exec(db, TCHAR_TO_ANSI ( * query ), 0, 0, &err) == SQLITE_OK ) {
		return true;

	} else {
		UE_LOG ( LogTemp, Error, TEXT ( "ASQLiteHandlerActor.CreateTable () : %s : There was error during : %s - creation : %s : %s ." ),
			* GetName (), * _name, * query, * FString ( err ) );
		return false;
	}
}

bool ASQLiteHandlerActor :: DeleteTable ( const FString _name ) {
	return true;
}

bool ASQLiteHandlerActor :: Insert ( const FString _nameTable, const TArray < FString > & _aColumns, const TArray < FString > & _aValues ) {
	char *err = 0;

	FString sColumns = "( ";
	int i = 0;

	if ( _aColumns.Num () > 0 ) {
		sColumns = sColumns + _aColumns [ 0 ];
	}

	for ( i = 1; i < _aColumns.Num (); i ++ ) {
		sColumns = sColumns + ", " + _aColumns [ i ];
	}

	sColumns = sColumns + " )";

	FString sValues = "( ";

	if ( _aValues.Num () > 0 ) {
		sValues = sValues + "'" + _aValues [ 0 ] + "'";
	}

	for ( i = 1; i < _aValues.Num (); i ++ ) {
		sValues = sValues + ", '" + _aValues [ i ] + "'";
	}

	sValues = sValues + " )";

	const FString query = "INSERT INTO " +
		_nameTable +
		" " + sColumns +
		" VALUES " + sValues + ";";

	// UE_LOG ( LogTemp, Log, TEXT ( "ASQLiteHandlerActor.Insert () : %s : Inserting data into : %s : %s ." ), * GetName (), * _nameTable, * query );

	if ( sqlite3_exec(db, TCHAR_TO_ANSI ( * query ), 0, 0, &err ) == SQLITE_OK ) {
		return true;

	} else {
		UE_LOG ( LogTemp, Error, TEXT ( "ASQLiteHandlerActor.Insert () : %s : There was error during data inserting into : %s : %s : %s ." ),
			* GetName (), * _nameTable, * query, * FString ( err ) );
		return false;
	}

	return true;
}

bool ASQLiteHandlerActor :: GetNewestValues_Internal ( const FString _query, TGetRowsCallbackFunctionalPointer _pfCallback ) {
	char *err = 0;

	// UE_LOG ( LogTemp, Log, TEXT ( "ASQLiteHandlerActor.GetNewestValues_Internal () : %s : Receiving data from : %s ." ), * GetName (), * _query );

	if ( sqlite3_exec ( db, TCHAR_TO_ANSI ( * _query ), _pfCallback, NULL, & err ) == SQLITE_OK ) {
		return true;

	} else {
		UE_LOG ( LogTemp, Error, TEXT ( "ASQLiteHandlerActor.GetNewestValues_Internal () : %s : There was error during data receiving from : %s : %s ." ),
			* GetName (), * _query, * FString ( err ) );
		return false;
	}
}

bool ASQLiteHandlerActor :: GetNewestValues ( const FString _nameTable, TGetRowsCallbackFunctionalPointer _pfCallback ) {
	const FString query = "SELECT * FROM " + _nameTable + " ORDER BY id DESC LIMIT 1;";
	return GetNewestValues_Internal ( query, _pfCallback );
}

bool ASQLiteHandlerActor :: GetNewestValuesByCity ( const FString _nameTable, const FString _city, TGetRowsCallbackFunctionalPointer _pfCallback ) {
	const FString query = "SELECT * FROM " + _nameTable + " WHERE city = '" + _city + "' ORDER BY id DESC LIMIT 1;";
	return GetNewestValues_Internal ( query, _pfCallback );
}

bool ASQLiteHandlerActor :: UpdateById ( const TArray < FString > & _aColumns, const TArray < FString > & _aValues, const FString _nameTable, const int _id ) {
	return true;
}

bool ASQLiteHandlerActor :: DeleteById ( const int _id ) {
	return true;
}
