// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// #pragma comment ( lib, "D:/Work/S, T and V, VR and AR and R/2020/5.18_Xsolla/Weather/Source/Weather/ThirdParty/Data/DataBases/sqlite3.lib" )

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SQLiteHandlerActor.generated.h"

// int ( * fpGetRowsCallback ) ( void *, int, char **, char ** ) = func_1;
typedef int ( * TGetRowsCallbackFunctionalPointer ) ( void *, int, char **, char ** );

UCLASS()
class WEATHER_API ASQLiteHandlerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASQLiteHandlerActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	void Initialize ();



	bool CreateTable ( const FString _name, const TArray < FString > & _aColumns );
	bool DeleteTable ( const FString _name );

	bool Insert ( const FString _nameTable, const TArray < FString > & _aColumns, const TArray < FString > & _aValues );
	bool GetNewestValues_Internal ( const FString _query, TGetRowsCallbackFunctionalPointer _pfCallback );
	bool GetNewestValues ( const FString _nameTable, TGetRowsCallbackFunctionalPointer _pfCallback );
	bool GetNewestValuesByCity ( const FString _nameTable, const FString _city, TGetRowsCallbackFunctionalPointer _pfCallback );
	bool UpdateById ( const TArray < FString > & _aColumns, const TArray < FString > & _aValues, const FString _nameTable, const int _id );
	bool DeleteById ( const int _id );
};
