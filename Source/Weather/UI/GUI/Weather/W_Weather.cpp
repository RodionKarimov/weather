// Fill out your copyright notice in the Description page of Project Settings.

#include "Logics/PCWeather.h"
#include "W_Weather.h"



UW_Weather :: UW_Weather ( const FObjectInitializer & ObjectInitializer )
	: Super ( ObjectInitializer )
{
	// InterfaceName = "W_Weather";
	
}

void UW_Weather :: NativeConstruct () {
	Super :: NativeConstruct ();

	if ( txtWeather ) {
		txtWeather -> SetText ( FText :: FromString ( "Weather weather weather weather." ) );
	}

	// APCWeather * pPC = Cast < APCWeather > ( GetOwningPlayer () );

	comboBox_City -> OnSelectionChanged.AddDynamic ( this, & UW_Weather::comboBox_City_OnSelectionChanged );
}

void UW_Weather :: SetWeather (
	float _temperature, float _temperatureFeelsLike, float _temperatureMin, float _temperatureMax,
	float _pressure, float _humidity,
	FString _description,
	FString _sWeatherMain, FString _sWeatherMainDescription,
	float _fVisibility,
	float _fWindSpeed, float _fWindDirection,
	int _iCloudsAll,
	FString _sDateAndTime
) {
	if ( txtWeather ) {
		// txtWeather -> SetText ( FText :: FromString ( _description ) );
		txtWeather -> SetText ( FText :: FromString (
			"Temperature : " + FString::SanitizeFloat ( _temperature ) + " , "
			"feels like : " + FString::SanitizeFloat ( _temperatureFeelsLike ) + " , "
			"min : " + FString::SanitizeFloat ( _temperatureMin ) + " , "
			"max : " + FString::SanitizeFloat ( _temperatureMax ) + " , "
			"pressure : " + FString::SanitizeFloat ( _pressure ) + " , "
			"humidity : " + FString::SanitizeFloat ( _humidity ) + " .\r\n" +
			_sWeatherMain + " - " + _sWeatherMainDescription + " .\r\n" +
			"Visibility : " + FString::SanitizeFloat ( _fVisibility ) + " .\r\n"
			"Wind : speed : " + FString::SanitizeFloat ( _fWindSpeed ) + " , "
			"direction : " + FString::SanitizeFloat ( _fWindDirection ) + " .\r\n"
			"Clouds all : " + FString::FromInt ( _iCloudsAll ) + " .\r\n"
			"Date and time : " + _sDateAndTime + " ."
		) );
	}
}

void UW_Weather :: ClearWeather () {
	if ( txtWeather ) {
		txtWeather -> SetText ( FText :: FromString ( "" ) );
	}
}

void UW_Weather :: comboBox_City_OnSelectionChanged ( FString _sSelectedItem, ESelectInfo :: Type _eSelectionType ) {
	APCWeather * pPC = Cast < APCWeather > ( GetOwningPlayer () );
	// int                                 i                   = -1;

	// i                                   = comboBox_City -> FindOptionIndex (_sSelectedItem);

	UE_LOG ( LogTemp, Log, TEXT ( "UW_Weather.comboBox_City_OnSelectionChanged () : %s : %s ." ),
		* GetName (),
		* ( _sSelectedItem )
	);

	pPC -> UpdateCity ( _sSelectedItem );
}
