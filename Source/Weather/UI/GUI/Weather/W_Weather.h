// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/ComboBoxString.h"

#include "W_Weather.generated.h"

/**
 * 
 */
UCLASS()
class WEATHER_API UW_Weather : public UUserWidget
{
	GENERATED_BODY()

	UW_Weather ( const FObjectInitializer & ObjectInitializer );
	virtual void NativeConstruct () override;

	UPROPERTY(meta = (BindWidget))
	UComboBoxString * comboBox_City;

	UPROPERTY(meta = (BindWidget))
	UTextBlock * txtWeather;

	UFUNCTION()
	void comboBox_City_OnSelectionChanged( FString _sSelectedItem, ESelectInfo::Type _eSelectionType );

public :

	void SetWeather (
    	float _temperature, float _temperatureFeelsLike, float _temperatureMin, float _temperatureMax,
		float _pressure, float _humidity,
		FString _description,
		FString _sWeatherMain, FString _sWeatherMainDescription,
		float _fVisibility,
		float _fWindSpeed, float _fWindDirection,
		int _iCloudsAll,
		FString _sDateAndTime
    );

    void ClearWeather ();
	
};
