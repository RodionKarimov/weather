// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using System.IO;
using UnrealBuildTool;

public class Weather : ModuleRules
{
	public Weather(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay",
			"UMG", "Slate", "SlateCore",
			"Http", "Json", "JsonUtilities"
		});

		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "../../ThirdParty/Data/DataBases"));
		PublicAdditionalLibraries.Add(Path.Combine(ModuleDirectory, "../../ThirdParty/Data/DataBases/sqlite3.lib"));
	}
}
