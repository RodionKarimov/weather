// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "WeatherGameMode.h"
// #include "Logics/PCWeather.h"
#include "WeatherCharacter.h"
#include "UObject/ConstructorHelpers.h"



AWeatherGameMode::AWeatherGameMode()
{
	TArray < AActor * > aActors;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// aActors.Empty ();
	// UGameplayStatics :: GetAllActorsOfClass ( ( const UObject * ) ( this -> GetWorld () ), ASQLiteHandlerActor :: StaticClass (), aActors );
	// if ( aActors.Num () == 0 ) {
	// 	UE_LOG ( LogTemp, Warning, TEXT ( "AWeatherGameMode.AWeatherGameMode () : There is no ASQLiteHandlerActor object in the scene." ) );
	// 	return;
	// } else {
	// 	m_pSQLiteHandlerActor = Cast < ASQLiteHandlerActor > ( aActors [ 0 ] );
	// }

	// if ( m_pSQLiteHandlerActor ) {
	// 	m_pSQLiteHandlerActor -> Initialize ();
	// }
}

void AWeatherGameMode :: BeginPlay () {
	// APCWeather * pPC;

	Super :: BeginPlay ();

	// pPC                                       = Cast < APCWeather > ( GetWorld()->GetGameInstance()->GetFirstLocalPlayerController() );
	// if ( pPC ) {
	// 	pPC -> initialize ();
	// }

	// if ( m_pWeatherHandler ) {
	// 	m_pWeatherHandler -> Initialize ( this );
	// }
}

// AWeatherHandler * AWeatherGameMode :: Get_WeatherHandler () {
// 	return m_pWeatherHandler;
// }

// ASQLiteHandlerActor * AWeatherGameMode :: Get_SQLiteHandlerActor () {
// 	return m_pSQLiteHandlerActor;
// }
