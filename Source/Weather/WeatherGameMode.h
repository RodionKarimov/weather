// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
// #include "System/Data/DataBases/SQLite/SQLiteHandlerActor.h"
#include "WeatherGameMode.generated.h"

UCLASS(minimalapi)
class AWeatherGameMode : public AGameModeBase
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

public:
	AWeatherGameMode();

	// AWeatherHandler * m_pWeatherHandler;
	// AWeatherHandler * Get_WeatherHandler ();

	// ASQLiteHandlerActor * m_pSQLiteHandlerActor;
	// ASQLiteHandlerActor * Get_SQLiteHandlerActor ();

};



